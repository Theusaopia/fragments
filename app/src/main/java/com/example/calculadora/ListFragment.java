package com.example.calculadora;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListFragment extends Fragment {
    private List<String> historico = new ArrayList<>();
    ListView listaHistorico;
    ArrayAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_historic, container, false);

        listaHistorico = (ListView) v.findViewById(R.id.listaHistorico);

        return v;
    }

    public void setHistorico(List<String> historico) {
        this.historico = historico;
        adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, this.historico);
        listaHistorico.setAdapter(adapter);
    }

    public void addToList(String entry) {
        historico.add(entry);
        adapter.notifyDataSetChanged();
    }

    public void clearList() {
        historico.clear();
        adapter.notifyDataSetChanged();
    }
}
