package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<String> historico;

    CalculationFragment fragCalculation;
    ListFragment fragList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragCalculation = (CalculationFragment) getFragmentManager().findFragmentByTag("fragCalculation");
        fragList = (ListFragment) getFragmentManager().findFragmentByTag("fragList");

        if(savedInstanceState != null) {
            historico = (ArrayList<String>) savedInstanceState.getSerializable("listaHistorico");
        }

        if(historico == null) {
            historico = new ArrayList<>();
        }

        fragList.setHistorico(historico);
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putSerializable("listaHistorico", (Serializable) historico);
    }

    public void calc (View v) {
        Double first = Double.parseDouble(fragCalculation.firstValue.getText().toString());
        Double second = fragCalculation.secondValue.getText().toString().length() > 0 ?  Double.parseDouble(fragCalculation.secondValue.getText().toString()) : 0;

        switch (v.getId()) {
            case R.id.sum:
                double sum = first + second;
                addToHistoric(buildHistoricString("+", String.valueOf(first), String.valueOf(second), String.valueOf(sum)));
                break;
            case R.id.minus:
                double minus = first - second;
                addToHistoric(buildHistoricString("-", String.valueOf(first), String.valueOf(second), String.valueOf(minus)));
                break;
            case R.id.division:
                double division = first / second;
                addToHistoric(buildHistoricString("/", String.valueOf(first), String.valueOf(second), String.valueOf(division)));
                break;
            case R.id.plus:
                double multiply = first * second;
                addToHistoric(buildHistoricString("*", String.valueOf(first), String.valueOf(second), String.valueOf(multiply)));
                break;
            case R.id.mod:
                if(!validatePercentage()) {
                    double mod = (first * 100) / second;
                    addToHistoric(buildHistoricString("%", String.valueOf(first), String.valueOf(second), String.valueOf(mod)));
                }
                break;
            case R.id.power:
                double power = Math.pow(first, second);
                addToHistoric(buildHistoricString("^", String.valueOf(first), String.valueOf(second), String.valueOf(power)));
                break;
            case R.id.squareRoot:
                if(!validateOneNumberCalcs()){
                    double square = Math.sqrt(first);
                    addToHistoric(buildHistoricString("√", String.valueOf(first), String.valueOf(square)));
                }
                break;
            case R.id.cubic:
                if(!validateOneNumberCalcs()){
                    double cubic = Math.cbrt(first);
                    addToHistoric(buildHistoricString("√", String.valueOf(first), String.valueOf(cubic)));
                }
                break;
            case R.id.clear:
                fragCalculation.firstValue.setText("");
                fragCalculation.secondValue.setText("");
                break;
            case R.id.clearAll:
                new AlertDialog.Builder(this)
                        .setTitle("Confirmar")
                        .setMessage("Quer realmente apagar o histórico?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                fragCalculation.firstValue.setText("");
                                fragCalculation.secondValue.setText("");
                                fragList.clearList();
                            }
                        })
                        .setNegativeButton("Não", null).show();
                break;
        }
    }

    public void addToHistoric(String entry) {
        fragList.addToList(entry);
    }

    public String buildHistoricString(String operator, String number1, String number2, String result) {
        return operator == "%" ? validateDecimalNumber(number1) + " de " + validateDecimalNumber(number2) + " = " + validateDecimalNumber(result) + "%"
                : validateDecimalNumber(number1) + " " + operator + " " + validateDecimalNumber(number2) + " = " + validateDecimalNumber(result);
    }

    public String buildHistoricString(String operator, String number1, String result) {
        return operator + validateDecimalNumber(number1) + " = " + validateDecimalNumber(result);
    }

    public String validateDecimalNumber(String number) {
        DecimalFormat format = new DecimalFormat("0.#");

        if(Double.parseDouble(number) % 1 == 0) {
            return format.format(Double.parseDouble(number));
        }else {
            return  String.format("%.2f", Double.parseDouble(number));
        }
    }

    public boolean validateOneNumberCalcs() {
        if(fragCalculation.secondValue.getText().toString().length() > 0) {
            Toast.makeText(this, "Este cálculo não usa o segundo valor", Toast.LENGTH_SHORT).show();
            return true;
        }

        return false;
    }

    public boolean validatePercentage() {
        if(Double.parseDouble(fragCalculation.firstValue.getText().toString()) > Double.parseDouble(fragCalculation.secondValue.getText().toString())) {
            Toast.makeText(this, "Primeiro valor maior que o segundo, impossível calcular a porcentagem", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }
}