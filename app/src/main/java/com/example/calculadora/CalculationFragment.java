package com.example.calculadora;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class CalculationFragment extends Fragment {
    EditText firstValue, secondValue;

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_calc_buttons, container, false);

        firstValue = (EditText) v.findViewById(R.id.edPrimeiroOperador);
        secondValue = (EditText) v.findViewById(R.id.edSegundoOperador);

        return v;
    }


}
